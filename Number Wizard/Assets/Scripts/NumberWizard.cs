﻿using UnityEngine;
using System.Collections;

public class NumberWizard : MonoBehaviour {

	const int MAX_START = 1000000000;
	const int MIN_START = 1;
	int max = MAX_START;
	int min = MIN_START;
	int guess;
	
	// Use this for initialization
	void Start () {
		StartGame();
	}
		
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.Return))
		{
			print ("Correct guess");
			StartGame ();
		}
		else if(Input.GetKeyDown(KeyCode.UpArrow))
		{
			min = guess;
			NextGuess();
		}
		else if(Input.GetKeyDown(KeyCode.DownArrow))
		{
			max = guess;
			NextGuess();
		}
	}
	
#region Game logic
	void StartGame()
	{
		max = MAX_START;
		min = MIN_START;

		print("===================================");
		print ("Welcome to Number Wizard");
		print ("Pick a number in your head, but don't tell me!");
		
		print ("The highest number you can pick is " + max);
		print ("The lowest number you can pick is " + min);
		
		NextGuess();
		
		max += 1;
	}
	
	void NextGuess()
	{
		guess = (max + min) / 2;
		print (string.Format("Is the number higher or lower than {0}?", guess));
		print ("Up arrow = higher, down = lower, return = equal");
	}
#endregion
}
