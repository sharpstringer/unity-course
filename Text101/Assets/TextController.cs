﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TextController : MonoBehaviour {
	
	public Text text;
	enum States { cell, window, door, corridor, corridor2, cell2, stairs, stairs2, freedom };
	private States gameState;	
	
	enum KeyStates{ none, rawMaterial, key };
	private KeyStates keyState;
	
	// Use this for initialization
	void Start () {
		text = gameObject.GetComponent<Text>();
		gameState = States.cell;
		keyState = KeyStates.none;
	}
	
	// Update is called once per frame
	void Update () {
		print (gameState);
		
		if(gameState == States.cell)
		{
			stateCell();
		}
		else if(gameState == States.window)
		{
			stateWindow();
		}
		else if(gameState == States.door)
		{
			stateDoor();
		}
		else if(gameState == States.corridor)
		{
			stateCorridor();
		}
		else if(gameState == States.corridor2)
		{
			stateCorridor2();
		}
		else if(gameState == States.stairs)
		{
			stateStairs();
		}		
		else if(gameState == States.stairs2)
		{
			stateStairs2();
		}
		else if(gameState == States.cell2)
		{
			stateCell2();
		}
		else if(gameState == States.freedom)
		{
			stateFreedom();
		}
	}
	
	void stateCell()
	{
		text.text = "You wake up on a hard floor in a cell. There are brick " +
				"walls, a small barred window and a metal door.\n\n" +
				"Press W to inspect Window\n" +
				"Press D to inspect Door";

		if(Input.GetKeyDown(KeyCode.W))
		{
			gameState = States.window;
		}
		else if(Input.GetKeyDown(KeyCode.D))
	    {
			gameState = States.door;			        
		}
	}

	void stateWindow ()
	{
		if(keyState == KeyStates.none)
		{
			text.text = "You see some grey buildings across the street. The bars look strong. " +
						"There is a loose piece of wood on the frame.\n\n" +
						"Press W to prise free the piece of Wood\n" +
						"Press R to Return";
						
			if(Input.GetKeyDown(KeyCode.W))
			{
				gameState = States.cell;
				keyState = KeyStates.rawMaterial;
			}
			else if(Input.GetKeyDown(KeyCode.R))
			{
				gameState = States.cell;
			}
		}
		else if(keyState == KeyStates.rawMaterial || keyState == KeyStates.key)
		{
			text.text = "You see some grey buildings across the street. The bars look strong. " +
						"There is a hole where you prised a piece of wood from the frame.\n\n" +
						"Press R to Return";
						
			if(Input.GetKeyDown(KeyCode.R))
			{
				gameState = States.cell;
			}
		}		
	}

	void stateDoor ()
	{
		if(keyState == KeyStates.none)
		{
			text.text = "The metal door is locked, and doesn't budge when you pull the handle. " +
						"It has a keyhole on the left hand side. Coming from under the door is a shadow from a set of keys. " +
						"There is only one key on the set, and could probably be crafted if you could find some material.\n\n" +
						"Press R to Return";
			
			if(Input.GetKeyDown(KeyCode.R))
			{
				gameState = States.cell;
			}
		}
		else if (keyState == KeyStates.rawMaterial)
		{
			text.text = "The metal door is locked, and doesn't budge when you pull the handle. " +
					"It has a keyhole on the left hand side. Coming from under the door is clear " +
					"shadow forming the outline of a key on a string.\n\n" +
					"Press C to place the piece of wood on the shadow and attempt to Craft a key\n" +
					"Press R to Return";
			
			if(Input.GetKeyDown(KeyCode.C))
			{
				keyState = KeyStates.key;
			}
			else if(Input.GetKeyDown(KeyCode.R))
			{
				gameState = States.cell;
			}
		}
		else if(keyState == KeyStates.key)
		{
			text.text = "The metal door is locked, but you have crafted an imperfect copy of the door key out of wood.\n\n" +
						"Press L to place the key in the Lock and attempt to open the door\n" +
						"Press R to Return";
						
			if(Input.GetKeyDown(KeyCode.L))
			{
				gameState = States.corridor;
			}
			else if(Input.GetKeyDown(KeyCode.R))
			{
				gameState = States.cell;
			}	
		}
	}

	void stateCorridor ()
	{
		text.text = "With door to your cell is open, you need to decide your next move. " +
					"You see another cell on the corridor, and a flight of stairs.\n\n" +
					"Press S to go to the Stairs\n" +
					"Press N to go to the Next cell";
		
		if(Input.GetKeyDown(KeyCode.S))
		{
			gameState = States.stairs;
		}
		if(Input.GetKeyDown(KeyCode.N))
		{
			gameState = States.cell2;
		}
	}
	
	void stateCorridor2 ()
	{
		text.text = "The stairs seem to be only realistic escape route.\n\n" +
					"Press S to go to the Stairs";
		
		if(Input.GetKeyDown(KeyCode.S))
		{
			gameState = States.stairs2;
		}
	}

	void stateCell2 ()
	{
		text.text = "The door is open, but the cell is empty.\n\n" +
					"Press R to Return to the corridor";
		
		if(Input.GetKeyDown(KeyCode.R))
		{
			gameState = States.corridor;
		}
	}
	
	void stateStairs ()
	{
		text.text = "You see a short set of stairs, with an old rickety door at the top. " +
					"You could try smashing the door down with brute force.\n\n" +
					"Press K to Kick the door hard\n" +
					"Press R to Return to the corridor";
		
		if(Input.GetKeyDown(KeyCode.K))
		{
			gameState = States.stairs2;
		}
		else if(Input.GetKeyDown(KeyCode.R))
		{
			gameState = States.corridor;
		}
	}
	
	void stateStairs2 ()
	{
		text.text = "The door looks weak, one more kick should do it!\n\n" +
					"Press K to Kick the weakened door\n" +
					"Press R to Return to the corridor";
		
		if(Input.GetKeyDown(KeyCode.R))
		{
			gameState = States.corridor2;
		}
		else if(Input.GetKeyDown(KeyCode.K))
		{
			gameState = States.freedom;
		}
	}
	

	void stateFreedom ()
	{
		text.text = "You have escaped the cell block and can feel a fresh breeze coming down the street\n" + 
					"You are free!\n\n" + 
					"Press P to Play again";
					
		if(Input.GetKeyDown(KeyCode.P))
		{
			gameState = States.cell;
			keyState = KeyStates.none;
		}
	}

}
